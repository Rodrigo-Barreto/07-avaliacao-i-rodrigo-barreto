
import React from "react";

import CardItem from "../../components/card/cardItem";
import { Grid, Box } from "@material-ui/core";


import "../../style.css"


class FavoriteView extends React.Component {


  

  render() {
    return (
      <>
        <div className='containerFavorite'>
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2}>
            </Grid>
          </Box>
          <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={2}>
              {this.props.favoritsStorage.map((item, index) =>
                <Grid item md={3} xs={12} sm={6}>
                  <CardItem index={index}
                    priceTotal={this.priceTotal} function={this.props.addFavorite} favoritsStorage={this.props.favoritsStorage} products={item} />
                </Grid>
              )}

            </Grid>
          </Box>

        </div>

      </>
    )
  }
}

export default FavoriteView;