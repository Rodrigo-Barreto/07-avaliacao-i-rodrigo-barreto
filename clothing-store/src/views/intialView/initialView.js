import React from "react";
import CardItem from "../../components/card/cardItem";
import "../../style.css"
import { Grid, Box } from "@material-ui/core";
import 'react-slideshow-image/dist/styles.css'
import SlideFull from "../../components/slide/slide";






class InitialView extends React.Component {


  render() {
   


    return <>
    <SlideFull/>
    <div className='fullP'>
      <div className='container'>
        <div>
        
        </div>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={1}>

          </Grid>
        </Box>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={1}>

            {this.props.products.map((item, index) =>
              <Grid item md={3} xs={12} sm={6}>
                <CardItem index={index}
                  favoritsStorage={this.props.favoritsStorage}
                  functionFavorite={this.props.functionFavorite}
                  function={this.props.addFavorite} products={item} />
              </Grid>
            )}

          </Grid>
        </Box>

      </div></div>
  
    </>

  }




}

export default InitialView;
