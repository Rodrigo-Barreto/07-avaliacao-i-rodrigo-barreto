import React from "react";
import { AppBar, Badge, Box, IconButton, makeStyles, Toolbar, } from "@material-ui/core";
import {  Favorite, } from "@material-ui/icons";
import './style.css';
import {
  useHistory
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  div: {
    flexGrow: 1
  },

  AppBar: {
    background: '#340617',
    height: 80,
    padding: 10,
    
  },

  logo: {
    height:80,
  }

}));


function TopBar(props) {
  const classes = useStyles();
  let history = useHistory();
  return (
    <AppBar className={classes.AppBar} >
      <Toolbar  >
     
        <div className={classes.div} />
        <img onClick={() => history.push("/category")} src="https://images.tcdn.com.br/img/img_prod/690339/1567106029_logo-site02.png" alt='logo' className={classes.logo} />
        <Box sx={{ flexGrow: 1 }} />
          <IconButton
            onClick={() => history.push("/favorites")}
            size="large"
            aria-label="show 4 new mails"
            color="inherit"
          >
            <Badge badgeContent={props.favoritsLent} color="error">
              <Favorite />
            </Badge>
          </IconButton>
         
        
     
      

      </Toolbar>
    </AppBar>
  );
}

export default TopBar;

// <IconButton
// onClick={() => history.push("/favorites")}
// size="large"
// aria-label="show 17 new notifications"
// color="inherit"
// >
// <Badge badgeContent={8} color="error">  {/*Aqui coloca a quantidade de arquivos add a lista de favoritos */}
//   <NotificationImportantOutlined />
// </Badge>

// </IconButton>

/* <IconButton
size="large"
edge="end"
aria-label="account of current user"

aria-haspopup="true"

color="inherit"
>
<AccountCircle />
</IconButton> */

/* <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
<IconButton
  onClick={() => history.push("/favorites")}
  size="large"
  aria-label="show 4 new mails"
  color="inherit"
>
  <Badge badgeContent={4} color="error">
    <Favorite />
  </Badge>
</IconButton> */

/* <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
<IconButton
  size="large"
  aria-label="show more"

  aria-haspopup="true"

  color="inherit"
>
  <More />
</IconButton>
</Box> */