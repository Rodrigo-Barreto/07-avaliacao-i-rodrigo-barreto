import React from "react";
import "../../style.css"
import { Slide } from 'react-slideshow-image';

 function SlideFull(){
    const slideImages = [
        'https://dafitistatic.dafiti.com.br/dynamic_yield/cms/static/dafiti/images/1b8d99f61ec85__destaque-masc-verao3.jpg',
        'https://dafitistatic.dafiti.com.br/dynamic_yield/cms/static/dafiti/images/3425ff58b79fb__destaque.jpg'
      ];
    return (
        <div className='fullP'><Slide easing="ease">
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[0]})` }}>
         
          </div>
        </div>
        <div className="each-slide">
          <div style={{ 'backgroundImage': `url(${slideImages[1]})` }}>
            
          </div>
        </div>
       
      </Slide>
      </div>
    );

 }

 export default SlideFull;