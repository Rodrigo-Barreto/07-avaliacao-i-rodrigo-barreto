import React from "react";
import "./style.css"
import InitialView from "./views/intialView/initialView";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import FavoriteView from "./views/favoriteView/favorite";
import TopBar from "./components/appBar/appBar";
import api from "./utils/api";
import localforage from "localforage";



class App extends React.Component {

  state = {
    favoritsStorage: [],
    products: [],
    isLoading:false,
   
  }

  async getProducts() {
    try {
      this.setState({isLoading:true})
      let response = await api.get('/products.json')
      
      this.setState({isLoading:false})
      let data = Object.entries(response.data);
      console.log(data)

      data.forEach((id) => {
        this.setState({
          products: [...this.state.products, {
            id: id[1].id,
            title: id[1].title,
            isFavorite: id[1].isFavorite,
            price: id[1].price,
            imageUrl: id[1].imageUrl,
            description: id[1].description
          }
          ]
        }
        )
      }
      )
    } catch (error) {
      alert(error.message)
    }
  }

  addFavorite = async (item) => {
    let teste = this.state.favoritsStorage.map((e) => e.id)
    if (!teste.includes(item.id)) {
     
      this.setState({
        favoritsStorage: [...this.state.favoritsStorage, item]
      })
    
    } else {
      let searc = this.state.favoritsStorage.indexOf(item)
      const { state: { favoritsStorage } } = this
      favoritsStorage.splice(searc, 1)
      this.setState({ favoritsStorage })
  
     
    }

  }

  isFavorite(id) {
    let teste = this.state.favoritsStorage.map((e) => e.id)
    return !teste.includes(id)
  }

  

  componentDidMount() {
    this.getProducts();
    
  }

  render() {
    return <>
      <Router >
        <TopBar favoritsLent={this.state.favoritsStorage.length}></TopBar>
        <Switch >
          <Route exact path="/">
            <div className="wrap">
              <InitialView
                isLoading={this.state.isLoading}
                getProducts={this.getProducts}
                favoritsStorage={this.state.favoritsStorage}
                products={this.state.products}
                addFavorite={this.addFavorite}
                favoritsLent={this.state.favoritsStorage.length} >
              </InitialView>
            </div>
          </Route>
          <Route path="/favorites">
            <div className="wrap">

              <FavoriteView
                addFavorite={this.addFavorite}
                isFavorite={this.isFavorite}
                favoritsStorage={this.state.favoritsStorage}
                favoritsLent={this.state.favoritsStorage.length}>
              </FavoriteView>
            </div>
          </Route>

        </Switch>

      </Router>
    </>
      ;
  }
}

export default App;
