import axios from "axios"

const api = axios.create({baseURL: "https://shoacc2-default-rtdb.firebaseio.com/"})

export default api;